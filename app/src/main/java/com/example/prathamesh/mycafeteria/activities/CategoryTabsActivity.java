package com.example.prathamesh.mycafeteria.activities;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.prathamesh.mycafeteria.CategoryFragment;
import com.example.prathamesh.mycafeteria.R;
import com.example.prathamesh.mycafeteria.models.OrderItem;
import com.example.prathamesh.mycafeteria.models.Product;
import com.example.prathamesh.mycafeteria.models.ProductModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryTabsActivity extends AppCompatActivity implements CategoryFragment.SendDataInterface{

    public static final String TAB_COUNT = "tabCount" ;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView textViewTotal, textViewViewCart, textViewTitle;
    private ImageView imageViewBack;
    private ProgressBar progressBar;
    private LinearLayout linearLayoutOrder;
    ArrayList<String> categories;

    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;

    ArrayList<ProductModel> productModelList;
    ArrayList<ProductModel> foodList;
    ArrayList<ProductModel> beverageList;
    ArrayList<ProductModel> cakeList;
    ArrayList<ProductModel> iceCreamList;
    ArrayList<ProductModel> chinsesList;
    ArrayList<ProductModel> comboList;


    int tab_count ;
    HashMap<String, Product> itemsHashMap = new HashMap<>();
    int total = 0;
    HashMap <String,Integer>  hashMap;
    //HashMap<String , ArrayList<ProductModel>> hashMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();
        setContentView(R.layout.activity_category_tabs);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        textViewTotal = (TextView) findViewById(R.id.textViewTotal);
        textViewViewCart = (TextView) findViewById(R.id.textViewViewCart);
        // imageViewBack = (ImageView)findViewById(R.id.imageViewBack) ;
        // textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        textViewViewCart.setEnabled(false);

        Intent intent = getIntent() ;
        tab_count = intent.getIntExtra(TAB_COUNT,0);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        linearLayoutOrder = (LinearLayout) findViewById(R.id.linearLayoutOrder);
        progressBar.setVisibility(View.VISIBLE);

        categories = new ArrayList<>();
        categories.add("Food");
        categories.add("Beverages");
        categories.add("Cakes");
        categories.add("Ice-Creams");
        categories.add("Chinese");
        categories.add("Combos");
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(
                tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        tabLayout.addTab(tabLayout.newTab().setText("Food"));
        tabLayout.addTab(tabLayout.newTab().setText("Beverages"));
        tabLayout.addTab(tabLayout.newTab().setText("Cakes"));
        tabLayout.addTab(tabLayout.newTab().setText("Ice-Creams"));
        tabLayout.addTab(tabLayout.newTab().setText("Chinese"));
        tabLayout.addTab(tabLayout.newTab().setText("Combos"));


        tabLayout.post(mTabLayout_config);

        textViewViewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoryTabsActivity.this, OrderSummaryActivity.class);
                Gson gson = new Gson();
                String hashmap = gson.toJson(itemsHashMap);
                intent.putExtra("Summary", hashmap);
                intent.putExtra("Total", total);
                startActivityForResult(intent, 100);
            }
        });



        mFirestore.collection("Products").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if(task.isSuccessful())
                {
                    productModelList = new ArrayList<>();

                    for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                        ProductModel productModel = documentSnapshot.toObject(ProductModel.class);
                        productModelList.add(productModel);
                    }
                    foodList = new ArrayList<>();
                    beverageList = new ArrayList<>();
                    cakeList= new ArrayList<>();
                    iceCreamList = new ArrayList<>();
                    chinsesList = new ArrayList<>();
                    comboList= new ArrayList<>();
                    for(int i =0; i<productModelList.size();i++)
                    {
                        if(productModelList.get(i).getCategory().matches("Food"))
                        {

                            foodList.add(productModelList.get(i));

                        }
                        if(productModelList.get(i).getCategory().matches("Beverages"))
                        {

                            beverageList.add(productModelList.get(i));
                        }
                        if(productModelList.get(i).getCategory().matches("Cakes"))
                        {

                            cakeList.add(productModelList.get(i));

                        }
                        if(productModelList.get(i).getCategory().matches("IceCreams"))
                        {

                            iceCreamList.add(productModelList.get(i));
                        }
                        if(productModelList.get(i).getCategory().matches("Chinese"))
                        {

                            chinsesList.add(productModelList.get(i));

                        }
                        if(productModelList.get(i).getCategory().matches("Combos"))
                        {

                            comboList.add(productModelList.get(i));
                        }

                    }

                    progressBar.setVisibility(View.GONE);
                    final PagerAdapter adapter = new PagerAdapter
                            (getSupportFragmentManager(), tabLayout.getTabCount());
                    viewPager.setAdapter(adapter);
                    viewPager.setCurrentItem(tab_count);
                    viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

//                    hashMap = new HashMap<>();
//                    hashMap.put("Food",foodList);
//                    hashMap.put("Beverages",beverageList);
                }

            }
        });
    }

    Runnable mTabLayout_config = new Runnable() {
        @Override
        public void run() {

            if (tabLayout.getWidth() < CategoryTabsActivity.this.getResources().getDisplayMetrics().widthPixels) {
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                ViewGroup.LayoutParams mParams = tabLayout.getLayoutParams();
                mParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                tabLayout.setLayoutParams(mParams);
            } else {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
        }
    };


    @Override
    public void getData(Product product) {

        if (product.getQuantity() == 0) {
            itemsHashMap.remove(product.getId());
        } else {
            itemsHashMap.put(product.getId(), product);
        }

                total = 0;
        ArrayList<String> productKeys = new ArrayList<>(itemsHashMap.keySet());
        for (String key : productKeys) {
            Product product1 = itemsHashMap.get(key);
            total = total + (product1.getQuantity() * Integer.parseInt(product1.getPrice()));
        }

        if (total > 0) {
            textViewViewCart.setEnabled(true);
            linearLayoutOrder.setVisibility(View.VISIBLE);

        } else {
            textViewViewCart.setEnabled(false);
            linearLayoutOrder.setVisibility(View.GONE);
        }

        textViewTotal.setText("Rs. " + total);


    }

    public int calculateTotal(HashMap<String, OrderItem> itemsHashMap) {
        int finalTotal = 0;

        ArrayList<String> itemKeys = new ArrayList<>(itemsHashMap.keySet());

        for (int i = 0; i < itemKeys.size(); i++) {
            OrderItem orderItem = (OrderItem) itemsHashMap.get(itemKeys.get(i));
            int total = orderItem.getQuantity() * orderItem.getPrice();
            finalTotal = finalTotal + total;
        }

        return finalTotal;
    }



    public class PagerAdapter extends FragmentPagerAdapter {

        int mNumOfTabs;
        ArrayList<Fragment> fragments;

        public PagerAdapter(FragmentManager fm, int mNumOfTabs) {
            super(fm);
            this.mNumOfTabs = mNumOfTabs;
            fragments = new ArrayList<>();
            for (int i = 0; i < mNumOfTabs; i++) {
                Bundle bundle = new Bundle();
                CategoryFragment categoryFragment;
                Gson gson = new Gson();
                if(i==0)
                {
                    categoryFragment = new CategoryFragment();
                    bundle.putString("category", "Food");
                    bundle.putString(CategoryFragment.KEY_PRODUCTS, gson.toJson(foodList));
                    categoryFragment.setArguments(bundle);
                    fragments.add(categoryFragment);
                }else if(i==1)
                {
                    categoryFragment = new CategoryFragment();
                    bundle.putString("category", "Beverages");
                    bundle.putString(CategoryFragment.KEY_PRODUCTS, gson.toJson(beverageList));
                    categoryFragment.setArguments(bundle);
                    fragments.add(categoryFragment);
                }
                else if(i==2)
                {
                    categoryFragment = new CategoryFragment();
                    bundle.putString("category", "Cakes");
                    bundle.putString(CategoryFragment.KEY_PRODUCTS, gson.toJson(cakeList));
                    categoryFragment.setArguments(bundle);
                    fragments.add(categoryFragment);
                }
                else if(i==3)
                {
                    categoryFragment = new CategoryFragment();
                    bundle.putString("category", "IceCreams");
                    bundle.putString(CategoryFragment.KEY_PRODUCTS, gson.toJson(iceCreamList));
                    categoryFragment.setArguments(bundle);
                    fragments.add(categoryFragment);
                }
                else if(i==4)
                {
                    categoryFragment = new CategoryFragment();
                    bundle.putString("category", "Chinese");
                    bundle.putString(CategoryFragment.KEY_PRODUCTS, gson.toJson(chinsesList));
                    categoryFragment.setArguments(bundle);
                    fragments.add(categoryFragment);
                }
                else if(i==5)
                {
                    categoryFragment = new CategoryFragment();
                    bundle.putString("category", "Combos");
                    bundle.putString(CategoryFragment.KEY_PRODUCTS, gson.toJson(comboList));
                    categoryFragment.setArguments(bundle);
                    fragments.add(categoryFragment);
                }
                else
                {
                    categoryFragment = new CategoryFragment();
                    bundle.putString("category", "");
                    bundle.putString(CategoryFragment.KEY_PRODUCTS, gson.toJson(new ArrayList<ProductModel>()));
                    categoryFragment.setArguments(bundle);
                    fragments.add(categoryFragment);
                }
            }

        }

        @Override
        public Fragment getItem(int p) {
            CategoryFragment categoryFragment = (CategoryFragment) fragments.get(p);
            Gson gson = new Gson();
            Bundle bundle = categoryFragment.getArguments();
            bundle.putString(CategoryFragment.KEY_CART, gson.toJson(itemsHashMap));
            return categoryFragment;
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
