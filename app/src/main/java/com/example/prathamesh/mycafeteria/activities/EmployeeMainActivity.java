package com.example.prathamesh.mycafeteria.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prathamesh.mycafeteria.R;
import com.example.prathamesh.mycafeteria.UserSharedPreference;
import com.example.prathamesh.mycafeteria.helpers.CustomLinearLayoutManager;
import com.example.prathamesh.mycafeteria.models.OrderHistoryItemModel;
import com.example.prathamesh.mycafeteria.models.PerItemOrder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class EmployeeMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView recyclerViewPending;
    private ProgressBar progressBar;

    LinearLayoutManager linearLayoutManager;

    CustomPendingAdapter customPendingAdapter;
    ArrayList<OrderHistoryItemModel> orderHistoryItems;
    FirebaseFirestore db;
    String userId;
    FirebaseAuth auth;
    FirebaseUser user;

    UserSharedPreference userSharedPreference;
    JSONArray jsonArray;
    OkHttpClient mClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mClient = new OkHttpClient();
        userSharedPreference = new UserSharedPreference(this);
        recyclerViewPending = (RecyclerView) findViewById(R.id.pendingList);
        linearLayoutManager = new LinearLayoutManager(this);

        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);


        recyclerViewPending.setLayoutManager(linearLayoutManager);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        db = FirebaseFirestore.getInstance();
        progressBar.setVisibility(View.VISIBLE);

        setTitle("Fetching...");

        getOrdersFromFirebase();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        recyclerViewPending.post(new Runnable()
//        {
//            @Override
//            public void run() {
//                customPendingAdapter.notifyDataSetChanged();
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.employee_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ehistory) {
           startActivity(new Intent(EmployeeMainActivity.this,EmployeeOrderHistory.class));

        } else if (id == R.id.nav_pending) {
            startActivity(new Intent(EmployeeMainActivity.this,EmployeeMainActivity.class));
            finish();

        } else if (id == R.id.nav_elogout) {

            FirebaseAuth.getInstance().signOut();
            userSharedPreference.clearSharedPreferences();
            startActivity(new Intent(EmployeeMainActivity.this,LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getOrdersFromFirebase() {
        setTitle("Fetching...");

        db.collection("Orders")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        progressBar.setVisibility(View.GONE);

                        orderHistoryItems = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            if (!document.getData().get("status").toString().matches("DELIVERED") && !document.getData().get("status").toString().matches("REJECTED")) {
                                OrderHistoryItemModel orderHistoryItemModel = document.toObject(OrderHistoryItemModel.class);
                                orderHistoryItemModel.setDocId(document.getId());
                                orderHistoryItems.add(orderHistoryItemModel);
                            }


                        }

                        int size = orderHistoryItems.size();
                        if(size < 1)
                        {
                            setTitle("No Order Pending");
                        }
                        if(size > 0)
                        {
                            setTitle(size+" Order Pending");
                        }
                        customPendingAdapter = new CustomPendingAdapter(EmployeeMainActivity.this, orderHistoryItems);

                        recyclerViewPending.setAdapter(customPendingAdapter);


                    }
                });


    }


    public class CustomPendingAdapter extends RecyclerView.Adapter<CustomPendingAdapter.ViewHolder> {

        private ArrayList<OrderHistoryItemModel> orderHistoryItems;
        private Context context;

        public CustomPendingAdapter(Context context, ArrayList<OrderHistoryItemModel> orderHistoryItems) {
            this.orderHistoryItems = orderHistoryItems;
            this.context = context;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_order_pending, parent, false);
            return new CustomPendingAdapter.ViewHolder(itemView);

        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


            SelectedItemsRecyclerAdapter selectedItemsRecyclerAdapter = new SelectedItemsRecyclerAdapter(orderHistoryItems.get(position).getPerItemOrder());
            RecyclerView.LayoutManager layoutManager = new CustomLinearLayoutManager(context);
            holder.mSelectedItemList.setLayoutManager(layoutManager);
            holder.mSelectedItemList.setAdapter(selectedItemsRecyclerAdapter);

            holder.tvOrderId.setText(orderHistoryItems.get(position).getOrderId().toUpperCase());

            String timestamp = orderHistoryItems.get(position).getDate();
            holder.tvDate.setText(timestamp.substring(0, timestamp.indexOf('T')));

            holder.tvTotal.setText("₹" + orderHistoryItems.get(position).getTotal());
            holder.tvAddress.setText(orderHistoryItems.get(position).getAddress());
            holder.tvPaymentMethod.setText(orderHistoryItems.get(position).getPaymentMethod());

            String status = orderHistoryItems.get(position).getStatus();
            jsonArray = new JSONArray();

            jsonArray.put(orderHistoryItems.get(position).getToken());
//            if(status.matches("REJECTED"))
//            {
//                try {
//                    orderHistoryItems.remove(holder.getAdapterPosition());
//                    notifyItemRemoved(holder.getAdapterPosition());
//                    notifyItemRangeRemoved(holder.getAdapterPosition(), orderHistoryItems.size());
//                    notifyItemChanged(holder.getAdapterPosition());
//                }
//                catch (Exception e)
//                {
//
//                }
//            }

            if (status.matches("PLACED")) {

                holder.b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Map<String, Object> map = new HashMap<>();
                        map.put("status", "ACCEPTED");


                        db.collection("Orders")
                                .document(orderHistoryItems.get(position).getDocId())
                                .update(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });




                        sendMessage(jsonArray, "Thanks for ordering...!!!", "welcome", "Http:\\google.com", "Your order accepted");

                    }
                });

                holder.b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Map<String, Object> map = new HashMap<>();
                        map.put("status", "REJECTED");


                        db.collection("Orders")
                                .document(orderHistoryItems.get(position).getDocId())
                                .update(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                        sendMessage(jsonArray, "Thanks for ordering...!!!", "welcome", "Http:\\google.com", "Your order has been rejected");


                    }
                });

            }

            if (status.matches("ACCEPTED")) {


                holder.b2.setBackgroundResource(R.drawable.button_main_selector);
                holder.b2.setText("Send Order");

                holder.b1.setText("CONTACT CUSTOMER");
                holder.b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        holder.b2.setBackgroundDrawable(null);
                        holder.b2.setText("On the way!");
                        Map<String, Object> map = new HashMap<>();
                        map.put("status", "PREPARED");


                        db.collection("Orders")
                                .document(orderHistoryItems.get(position).getDocId())
                                .update(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });

                        sendMessage(jsonArray, "Thanks for ordering...!!!", "welcome", "Http:\\google.com", "Your order has been sent");

                    }
                });

                holder.b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + orderHistoryItems.get(position).getMobile()));
                        startActivity(callIntent);
                    }
                });

            }

            if (status.matches("PREPARED")) {

                holder.b2.setBackgroundDrawable(null);
                holder.b2.setText("On the way!");
                holder.b1.setText("CONTACT CUSTOMER");

                holder.b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + orderHistoryItems.get(position).getMobile()));
                        startActivity(callIntent);
                    }
                });

            }


        }







    public void sendMessage(final JSONArray recipients, final String title, final String body, final String icon, final String message) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject root = new JSONObject();
                    JSONObject notification = new JSONObject();
                    notification.put("body", body);
                    notification.put("title", title);
                    notification.put("icon", icon);

                    JSONObject data = new JSONObject();
                    data.put("message", message);
                    root.put("notification", notification);
                    root.put("data", data);
                    root.put("registration_ids", recipients);

                    String result = postToFCM(root.toString());
                    Log.d("Main Activity", "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    Toast.makeText(EmployeeMainActivity.this, "Message Success: " + success + "Message Failed: " + failure, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("ERR",e.getMessage());
                    Toast.makeText(EmployeeMainActivity.this, "Message Failed, Unknown error occurred."+e, Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    String postToFCM(String bodyString) throws IOException {



        final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + "AAAAA8m5SkI:APA91bHyvCQTOS6GeQzklgAD3szVcOvqKuLDPMWo7JfZVuhj9R8NLiIY5w6qInMUo4DFmRBpDPfs_ug8CTH4pVl82xYbJmlIB5KWlhCf3JAZY4wbigXssDAQozxrWrMvZWl5xsvR4pOn")
                .build();
        okhttp3.Response response = mClient.newCall(request).execute();
        return response.body().string();
    }

        @Override
        public int getItemCount() {
            return orderHistoryItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {


            TextView tvOrderId, tvDate, tvTotal, tvAddress, tvPaymentMethod;
            RecyclerView mSelectedItemList;
            Button b1, b2;


            public ViewHolder(View itemView) {
                super(itemView);

                tvOrderId = (TextView) itemView.findViewById(R.id.tvOrderId);
                tvDate = (TextView) itemView.findViewById(R.id.tvDate);
                tvTotal = (TextView) itemView.findViewById(R.id.tvTotal);
                tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
                tvPaymentMethod = (TextView) itemView.findViewById(R.id.tvPaymentMethod);
                mSelectedItemList = (RecyclerView) itemView.findViewById(R.id.selected_items_list);
                b1 = (Button) itemView.findViewById(R.id.b1);
                b2 = (Button) itemView.findViewById(R.id.b2);


            }


        }
    }

    public class SelectedItemsRecyclerAdapter extends RecyclerView.Adapter<SelectedItemsRecyclerAdapter.ViewHolder> {

        public List<PerItemOrder> perItemOrders;

        public SelectedItemsRecyclerAdapter(List<PerItemOrder> perItemOrders) {
            this.perItemOrders = perItemOrders;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_selected_item, parent, false);

            return new SelectedItemsRecyclerAdapter.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            if (perItemOrders != null) {
                holder.name.setText(perItemOrders.get(position).getName());
                holder.quant.setText(perItemOrders.get(position).getQuantity() + "");
                holder.rate.setText("₹" + perItemOrders.get(position).getPricePerItem());
            }
        }

        @Override
        public int getItemCount() {
            return perItemOrders.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView name;
            public TextView quant;
            public TextView rate;

            public ViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.item_name);
                quant = (TextView) itemView.findViewById(R.id.item_quant);
                rate = (TextView) itemView.findViewById(R.id.item_rate);
            }
        }
    }
}
