package com.example.prathamesh.mycafeteria.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prathamesh.mycafeteria.R;
import com.example.prathamesh.mycafeteria.UserSharedPreference;
import com.example.prathamesh.mycafeteria.models.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Random;

public class LoginActivity extends AppCompatActivity {

    TextView tvSignup;
    Button buttonLogin;
    EditText editTextEmail, editTextPassword;
    TextView tvForgotPassword;


    private FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;

     String email;
     String  role,address,mobile;

    private ProgressDialog progressDialog;

    UserSharedPreference userSharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userSharedPreference = new UserSharedPreference(this);

        tvSignup= (TextView)findViewById(R.id.tvSignup);

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();

        progressDialog= new ProgressDialog(this);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonLogin = (Button) findViewById(R.id.buttonlogin);
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,SignupActivity.class);
                startActivity(intent);
            }
        });
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
            }
        });
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
                finish();
            }
        });

        if (user != null) {
            if(userSharedPreference.getRole().matches("Customer")) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
            if(userSharedPreference.getRole().matches("Employee")) {
                startActivity(new Intent(LoginActivity.this, EmployeeMainActivity.class));
                finish();
            }
        }

        orderNo();


    }


    public void userLogin() {


        email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (email.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("Email is a required field");
        }
        if (password.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("Password is a required field");
        }
        if(email.equals("admin") && password.equals("admin"))
        {
            Intent i = new Intent(LoginActivity.this, AdminMainActivity.class);
            //i.putExtra(ShowComplaintActivity.CHECK,"admin");
            startActivity(i);
        }
        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }


        else {
            progressDialog.setMessage("Log In...");
            progressDialog.show();
            progressDialog.setCancelable(false);

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                               getUserData();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
    public String orderNo() {

        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
           char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        Log.d("ORDER_NO", String.valueOf(sb1.insert(0,"O-")));
        return sb1.toString();
    }

    public void getUserData()
    {

        mFirestore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful()) {


                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                                if (email.matches((String) documentSnapshot.getData().get("email")))
                                {
                                    role = (String) documentSnapshot.getData().get("role");
                                    address = (String) documentSnapshot.getData().get("address");
                                    mobile = (String) documentSnapshot.getData().get("mobile");

                                    userSharedPreference.setRole(role);
                                    userSharedPreference.setAddress(address);
                                    userSharedPreference.setMobile(mobile);
                                    if(role.matches("Customer"))
                                    {
                                        progressDialog.dismiss();


                                        Toast.makeText(LoginActivity.this, "Login Successfull.", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                        finish();
                                    }else {
                                        progressDialog.dismiss();
                                        startActivity(new Intent(LoginActivity.this, EmployeeMainActivity.class));
                                        finish();
                                        Toast.makeText(LoginActivity.this, "Login Employee.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }
                        }
                    }
                });
    }

}
