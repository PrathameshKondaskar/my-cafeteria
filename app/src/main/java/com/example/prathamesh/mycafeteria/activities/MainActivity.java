package com.example.prathamesh.mycafeteria.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.prathamesh.mycafeteria.MyFirebaseMessagingService;
import com.example.prathamesh.mycafeteria.R;
import com.example.prathamesh.mycafeteria.UserSharedPreference;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String NEW_LOGIN = "newLogin" ;
    CardView cardViewFood,cardViewBeverages,cardViewCakes,cardViewIcecreams,cardViewChinese,cardViewCombos;
    UserSharedPreference userSharedPreference;


    OkHttpClient mClient ;



    JSONArray jsonArray ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        cardViewFood = (CardView)findViewById(R.id.cardFood);
        cardViewBeverages = (CardView)findViewById(R.id.cardBeverages);
        cardViewCakes = (CardView)findViewById(R.id.cardCakes);
        cardViewIcecreams = (CardView)findViewById(R.id.cardIcecream);
        cardViewChinese=(CardView)findViewById(R.id.cardChinese);
        cardViewCombos =(CardView)findViewById(R.id.cardCombos);
        mClient = new OkHttpClient();
        jsonArray = new JSONArray();
        setSupportActionBar(toolbar);

userSharedPreference = new UserSharedPreference(this);
        cardViewFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,CategoryTabsActivity.class);
                intent.putExtra(CategoryTabsActivity.TAB_COUNT,0);
                startActivity(intent);
            }
        });
        cardViewBeverages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,CategoryTabsActivity.class);
                intent.putExtra(CategoryTabsActivity.TAB_COUNT,1);
                startActivity(intent);
            }
        });
        cardViewCakes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,CategoryTabsActivity.class);
                intent.putExtra(CategoryTabsActivity.TAB_COUNT,2);
                startActivity(intent);
            }
        });cardViewIcecreams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,CategoryTabsActivity.class);
                intent.putExtra(CategoryTabsActivity.TAB_COUNT,3);
                startActivity(intent);
            }
        });cardViewChinese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,CategoryTabsActivity.class);
                intent.putExtra(CategoryTabsActivity.TAB_COUNT,4);
                startActivity(intent);
            }
        });cardViewCombos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,CategoryTabsActivity.class);
                intent.putExtra(CategoryTabsActivity.TAB_COUNT,5);
                startActivity(intent);
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


      Intent intent = getIntent();

      if(intent.getStringExtra(NEW_LOGIN)!= null) {
          jsonArray = new JSONArray();

          jsonArray.put(userSharedPreference.getToken());

          sendMessage(jsonArray, "Welcome to cafeteria", "welcome", "Http:\\google.com", ":-)");
      }


    }

    public void sendMessage(final JSONArray recipients, final String title, final String body, final String icon, final String message) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject root = new JSONObject();
                    JSONObject notification = new JSONObject();
                    notification.put("body", body);
                    notification.put("title", title);
                    notification.put("icon", icon);

                    JSONObject data = new JSONObject();
                    data.put("message", message);
                    root.put("notification", notification);
                    root.put("data", data);
                    root.put("registration_ids", recipients);

                    String result = postToFCM(root.toString());
                    Log.d("Main Activity", "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    Toast.makeText(MainActivity.this, "Message Success: " + success + "Message Failed: " + failure, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("ERR",e.getMessage());
                    Toast.makeText(MainActivity.this, "Message Failed, Unknown error occurred."+e, Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    String postToFCM(String bodyString) throws IOException {



        final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + "AAAAA8m5SkI:APA91bHyvCQTOS6GeQzklgAD3szVcOvqKuLDPMWo7JfZVuhj9R8NLiIY5w6qInMUo4DFmRBpDPfs_ug8CTH4pVl82xYbJmlIB5KWlhCf3JAZY4wbigXssDAQozxrWrMvZWl5xsvR4pOn")
                .build();
        okhttp3.Response response = mClient.newCall(request).execute();
        return response.body().string();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_chistory) {
            startActivity(new Intent(MainActivity.this,CustomerOrderHistoryActivity.class));

        } else if (id == R.id.nav_home) {
            startActivity(new Intent(MainActivity.this,MainActivity.class));
            finish();
        } else if (id == R.id.nav_menu) {
            startActivity(new Intent(MainActivity.this,CategoryTabsActivity.class));


        } else if (id == R.id.nav_clogout) {

            FirebaseAuth.getInstance().signOut();
            userSharedPreference.clearSharedPreferences();
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
