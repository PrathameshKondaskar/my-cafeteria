package com.example.prathamesh.mycafeteria.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prathamesh.mycafeteria.R;
import com.example.prathamesh.mycafeteria.UserSharedPreference;
import com.example.prathamesh.mycafeteria.helpers.DateFormatHelper;
import com.example.prathamesh.mycafeteria.models.OrderItem;
import com.example.prathamesh.mycafeteria.models.OrdersModel;
import com.example.prathamesh.mycafeteria.models.PerItemOrder;
import com.example.prathamesh.mycafeteria.models.Product;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class OrderSummaryActivity extends AppCompatActivity {


    ArrayList<Product>products;
    //VIEWS
    private LinearLayout linearLayout;
    private TextView textViewOrderItem, textViewOrderQuantity, textViewTotal, textViewOrderItemPrice, textViewTitle;
    private View view;
    private Button buttonPlaceOrder;
    private RadioGroup radioGroup;
    private RadioButton rbCOD,rbCREDIT,rbDEBIT;
    private TextView tvAddress;

    int total;
    AlertDialog dialog = null;
    UserSharedPreference userSharedPreference;
    private Context context = this;
    List<PerItemOrder> perItemOrders;

    HashMap<String, Product> itemsHashMap;
    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;

    String paymentType= "COD",orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();

        userSharedPreference = new UserSharedPreference(this);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        textViewTotal = (TextView) findViewById(R.id.textViewTotal);
        buttonPlaceOrder = (Button) findViewById(R.id.buttonPlaceOrder);
        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        rbCOD = (RadioButton)findViewById(R.id.rbCOD);
        rbCREDIT = (RadioButton)findViewById(R.id.rbCREDIT);
        rbDEBIT = (RadioButton)findViewById(R.id.rbDEBIT);
        tvAddress = (TextView)findViewById(R.id.tvAddress);
        tvAddress.setText(userSharedPreference.getAddress());

        rbCOD.setChecked(true);
        final Intent intent = getIntent();
        total = intent.getIntExtra("Total", 0);

        orderId = orderNo();
        textViewTotal.setText("RS." + total);
        Gson gson = new Gson();
        Type hashMapType = new TypeToken<HashMap<String,Product>>() {
        }.getType();
        itemsHashMap = gson.fromJson(intent.getStringExtra("Summary"), hashMapType);
        final ArrayList<String> productKeys = new ArrayList<>(itemsHashMap.keySet());


        perItemOrders = new ArrayList<>();
        products = new ArrayList<>();


        for(int i = 0; i<productKeys.size();i++)
        {
            Product orderItem = itemsHashMap.get(productKeys.get(i));
            products.add(orderItem);
        }
        addView();

        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("TAG", "Here");

                for(int i =0 ; i<productKeys.size();i++)
                {
                    Product product = itemsHashMap.get(productKeys.get(i));
                    products.add(product);

                    PerItemOrder perItemOrder = new PerItemOrder(products.get(i).getName(), products.get(i).getQuantity(),Integer.parseInt( products.get(i).getPrice()));

                    perItemOrders.add(perItemOrder);


                }

                saveOrderToFirebase();

            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {

                if(id == R.id.rbCOD)
                {
                    paymentType = "COD";
                }
                if(id == R.id.rbCREDIT)
                {
                    paymentType = "CREDIT";
                }
                if(id == R.id.rbDEBIT)
                {
                    paymentType = "DEBIT";
                }

                Log.d("PAYMENT",paymentType);
            }
        });
    }



    public void saveOrderToFirebase() {

        OrdersModel ordersModel = new OrdersModel(perItemOrders, user.getUid(), total, DateFormatHelper.getISOString(new Date()),"PLACED",paymentType,userSharedPreference.getAddress(),orderId,userSharedPreference.getMobile(),userSharedPreference.getToken());
        mFirestore.collection("Orders").add(ordersModel)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(context, "data added", Toast.LENGTH_SHORT).show();
                            if(paymentType.matches("COD"))
                            {
                                LayoutInflater inflater = (LayoutInflater) OrderSummaryActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final AlertDialog.Builder mBuild = new AlertDialog.Builder(OrderSummaryActivity.this);
                                final View mv = inflater.inflate(R.layout.content_progress, null);
                                final ProgressBar progressBar = (ProgressBar) mv.findViewById(R.id.progress);
                                final Button buttonOk = (Button) mv.findViewById(R.id.buttonOk);
                                final TextView textViewOrderPlacing = (TextView) mv.findViewById(R.id.textViewOrderPlacing);
                                final TextView textViewSuccess = (TextView) mv.findViewById(R.id.textViewSuccess);
                                textViewOrderPlacing.setVisibility(View.VISIBLE);
                                buttonOk.setVisibility(View.GONE);
                                mBuild.setView(mv);
                                mBuild.setCancelable(false);
                                dialog = mBuild.create();
                                dialog.show();
                                dialog.setCanceledOnTouchOutside(false);
                                final Handler handler = new Handler();
                                final Timer timer = new Timer();
                                final Runnable runnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
                                        textViewOrderPlacing.setVisibility(View.GONE);
                                        textViewSuccess.setVisibility(View.VISIBLE);
                                        buttonOk.setVisibility(View.VISIBLE);
                                        timer.cancel();
                                        buttonOk.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                dialog.dismiss();
                                                Intent intent1 = new Intent(OrderSummaryActivity.this, MainActivity.class);
                                                startActivity(intent1);
                                                setResult(100, intent1);
                                                finish();
                                            }
                                        });

                                    }
                                };

                                timer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        handler.post(runnable);
                                    }
                                }, 4000, 1000);


                            }else {

                                startActivity(new Intent(OrderSummaryActivity.this, CreditCardActivity.class));
                            }
                        } else {
                            Toast.makeText(context, "data not added", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


    public void addView() {

        LayoutInflater inflater = LayoutInflater.from((context));


        // textViewOrder.setText(orderModelList.get(position).getName());
        Log.d("xxxxx", String.valueOf(products.size()));
        for (int i = 0; i < products.size(); i++) {
            view = inflater.inflate(R.layout.custom_ordersummary_list, null, false);
            textViewOrderItem = (TextView) view.findViewById(R.id.textViewOrderItem);
            textViewOrderQuantity = (TextView) view.findViewById(R.id.textViewOrderQuantity);
            textViewOrderItemPrice = (TextView) view.findViewById(R.id.textViewOrderItemPrice);
            if (products.get(i).getQuantity() > 0) {
                textViewOrderItem.setText(products.get(i).getName());
                textViewOrderItemPrice.setText("Rs." + products.get(i).getPrice() + "/-");
                textViewOrderQuantity.setText(products.get(i).getQuantity() + "");
                linearLayout.addView(view);
            }

        }

    }
    public String orderNo() {

        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
      //  Log.d("ORDER_NO", String.valueOf(sb1.insert(0,"O-")));
        return sb1.insert(0,"O-").toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
