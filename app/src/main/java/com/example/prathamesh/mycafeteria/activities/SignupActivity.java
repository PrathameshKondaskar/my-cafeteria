package com.example.prathamesh.mycafeteria.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prathamesh.mycafeteria.R;
import com.example.prathamesh.mycafeteria.UserSharedPreference;
import com.example.prathamesh.mycafeteria.models.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

public class SignupActivity extends AppCompatActivity {

    //Views
    EditText editTextName,editTextMobile,editTextEmail,editTextPassword,editTextAddress;
    Button buttonSignup;
    TextView tvLogin;
    Spinner spinnerRole;

    //Firebase Variables;
    private FirebaseAuth mAuth;
    FirebaseFirestore mFireStore;

    //Model
    UserInfoModel userInfo;

    //Variables
    ArrayList<String> roleList;
    String role;
    UserSharedPreference userSharedPreference;

    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        FirebaseApp.initializeApp(SignupActivity.this);

        progressDialog= new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        userSharedPreference = new UserSharedPreference(this);

        editTextName=(EditText)findViewById(R.id.editTextName);
        editTextMobile=(EditText)findViewById(R.id.editTextMobile);
        editTextEmail=(EditText)findViewById(R.id.editTextEmail);
        editTextPassword=(EditText)findViewById(R.id.editTextPassword);
        editTextAddress=(EditText)findViewById(R.id.editTextAddress);
        buttonSignup=(Button)findViewById(R.id.buttonSignup);
        tvLogin=(TextView) findViewById(R.id.tvLogin);
        spinnerRole = (Spinner)findViewById(R.id.spinnerRole);
        roleList = new ArrayList<>();
        roleList.add("Customer");
        roleList.add("Employee");
//        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                int posi = spinnerRole.getSelectedItemPosition();
                if (posi == 0 ) {
                    role = "Customer";
                } else {
                    role = "Employee";
                }

                Log.d("Role", role);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        CustomAdapter customAdapter = new CustomAdapter(SignupActivity.this, android.R.layout.simple_list_item_1,roleList);
        spinnerRole.setAdapter(customAdapter);

        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    public void registerUser() {
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String fullName = editTextName.getText().toString();
        final String mobile = editTextMobile.getText().toString();
        final String address = editTextAddress.getText().toString();


        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (fullName.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextName;
            editTextName.setError("Full name is a required field");
        }
        if (password.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("Password is a required field");

        }
        if (address.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextAddress;
            editTextAddress.setError("Address is a required field");

        }
        if (password.length() < 6) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("Password must be greater than 6 letters");
        }

        if (mobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a required field");
        }
        if (!mobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || mobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. must be of 10 digit");
        }
        if (email.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is not valid");
        }

        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {
            firebaseLogin(fullName,email,password,role,mobile,address);
        }
    }

    public void firebaseLogin(final String name, final String email , final String password, final String role, final String mobile, final String address)
    {

        progressDialog.setMessage("Sign Up...");
        progressDialog.show();
        progressDialog.setCancelable(false);

        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                           // Toast.makeText(SignupActivity.this, "Authentication Successfull.", Toast.LENGTH_SHORT).show();
                            String userId = mAuth.getUid();
                            userInfo = new UserInfoModel(name,email,password,role,mobile,address,userId);

                            mFireStore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO).add(userInfo)
                                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentReference> task) {
                                            if(task.isSuccessful())
                                            {
                                                progressDialog.dismiss();
                                                userSharedPreference.setRole(role);
                                                if(role.matches("Customer"))
                                                {

                                                    FirebaseInstanceId.getInstance().getInstanceId()
                                                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                                    if (!task.isSuccessful()) {
                                                                        Log.w("ERR", "getInstanceId failed", task.getException());
                                                                        return;
                                                                    }

                                                                    // Get new Instance ID token
                                                                    String token = task.getResult().getToken();
                                                                    // Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
                                                                    if(userSharedPreference.getToken()==null)
                                                                    {

                                                                        userSharedPreference.setToken(token);

                                                                    }
                                                                }
                                                            });
                                                    Toast.makeText(SignupActivity.this, "Signup Successful", Toast.LENGTH_LONG).show();
                                                    Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                                    intent.putExtra(MainActivity.NEW_LOGIN,"newLogin");
                                                    startActivity(intent);
                                                    finish();
                                                }else
                                                { Toast.makeText(SignupActivity.this, "Signup Successful", Toast.LENGTH_LONG).show();
                                                    startActivity(new Intent(SignupActivity.this, EmployeeMainActivity.class));
                                                    finish();

                                                }

                                            } else {
                                                progressDialog.dismiss();
                                                Log.d("ERR",task.getException().toString());
                                                Toast.makeText(SignupActivity.this, "data not added", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });

                        }
                        else {
                            progressDialog.dismiss();
                            if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                Toast.makeText(SignupActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



    public class CustomAdapter extends ArrayAdapter<String> implements SpinnerAdapter {
        Context context;
        ArrayList<String> paymentMode = new ArrayList<>();

        public CustomAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
            super(context, resource, objects);
            this.context = context;
            this.paymentMode = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


            View view = super.getView(position, convertView, parent);

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(getResources().getColor(R.color. black));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER);
            Typeface typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_semibold);
            textView.setTypeface(typeface);
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            convertView = LayoutInflater.from(SignupActivity.this).inflate(
                    R.layout.custom_spinner, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(paymentMode.get(position));

            return convertView;
        }
    }
}
