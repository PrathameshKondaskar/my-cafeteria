package com.example.prathamesh.mycafeteria.models;

import java.util.List;

public class OrdersModel {

    List<PerItemOrder> perItemOrder ;
    String userId;
    int total;
    String date;
    String status,paymentMethod,address,orderId,mobile;
    String token;

    public OrdersModel(List<PerItemOrder> perItemOrder, String userId, int total, String date, String status, String paymentMethod, String address, String orderId, String mobile, String token) {
        this.perItemOrder = perItemOrder;
        this.userId = userId;
        this.total = total;
        this.date = date;
        this.status = status;
        this.paymentMethod = paymentMethod;
        this.address = address;
        this.orderId = orderId;
        this.mobile = mobile;
        this.token = token;
    }

    public List<PerItemOrder> getPerItemOrder() {
        return perItemOrder;
    }

    public void setPerItemOrder(List<PerItemOrder> perItemOrder) {
        this.perItemOrder = perItemOrder;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
