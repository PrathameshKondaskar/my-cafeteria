package com.example.prathamesh.mycafeteria.models;

public class PerItemOrder {

    private String name;
    private int quantity, pricePerItem;

    public PerItemOrder() {
    }

    public PerItemOrder(String name, int quantity, int pricePerItem) {
        this.name = name;
        this.quantity = quantity;
        this.pricePerItem = pricePerItem;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPricePerItem() {
        return pricePerItem;
    }

    public void setPricePerItem(int pricePerItem) {
        this.pricePerItem = pricePerItem;
    }

}
